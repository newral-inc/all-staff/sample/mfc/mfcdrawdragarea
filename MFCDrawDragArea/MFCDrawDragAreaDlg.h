
// MFCDrawDragAreaDlg.h : ヘッダー ファイル
//

#pragma once
#include "afxwin.h"


// CMFCDrawDragAreaDlg ダイアログ
class CMFCDrawDragAreaDlg : public CDialogEx
{
// コンストラクション
public:
	CMFCDrawDragAreaDlg(CWnd* pParent = NULL);	// 標準コンストラクター

// ダイアログ データ
	enum { IDD = IDD_MFCDRAWDRAGAREA_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV サポート


// 実装
protected:
	HICON m_hIcon;

	// 生成された、メッセージ割り当て関数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);

private:
	CStatic m_pcImage;	// ピクチャーコントロール

private:
	BOOL m_bMouseDown;	// マウスの左ボタンが押下中かを示すフラグ
	double m_startX;	// マウスの左ボタンが押下された時のX座標
	double m_startY;	// マウスの左ボタンが押下された時のY座標
	double m_endX;		// マウスが移動中のX座標
	double m_endY;		// マウスが移動中のY座標
};
