
// MFCDrawDragArea.h : PROJECT_NAME アプリケーションのメイン ヘッダー ファイルです。
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"		// メイン シンボル


// CMFCDrawDragAreaApp:
// このクラスの実装については、MFCDrawDragArea.cpp を参照してください。
//

class CMFCDrawDragAreaApp : public CWinApp
{
public:
	CMFCDrawDragAreaApp();

// オーバーライド
public:
	virtual BOOL InitInstance();

// 実装

	DECLARE_MESSAGE_MAP()
};

extern CMFCDrawDragAreaApp theApp;