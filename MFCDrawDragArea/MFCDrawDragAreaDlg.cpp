
// MFCDrawDragAreaDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "MFCDrawDragArea.h"
#include "MFCDrawDragAreaDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// アプリケーションのバージョン情報に使われる CAboutDlg ダイアログ

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// ダイアログ データ
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

// 実装
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMFCDrawDragAreaDlg ダイアログ



CMFCDrawDragAreaDlg::CMFCDrawDragAreaDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMFCDrawDragAreaDlg::IDD, pParent)
	, m_bMouseDown(FALSE)
	, m_startX(0.0)
	, m_startY(0.0)
	, m_endX(0.0)
	, m_endY(0.0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCDrawDragAreaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PC_IMAGE, m_pcImage);
}

BEGIN_MESSAGE_MAP(CMFCDrawDragAreaDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_DRAWITEM()
END_MESSAGE_MAP()


// CMFCDrawDragAreaDlg メッセージ ハンドラー

BOOL CMFCDrawDragAreaDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// "バージョン情報..." メニューをシステム メニューに追加します。

	// IDM_ABOUTBOX は、システム コマンドの範囲内になければなりません。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// このダイアログのアイコンを設定します。アプリケーションのメイン ウィンドウがダイアログでない場合、
	//  Framework は、この設定を自動的に行います。
	SetIcon(m_hIcon, TRUE);			// 大きいアイコンの設定
	SetIcon(m_hIcon, FALSE);		// 小さいアイコンの設定

	// TODO: 初期化をここに追加します。

	return TRUE;  // フォーカスをコントロールに設定した場合を除き、TRUE を返します。
}

void CMFCDrawDragAreaDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// ダイアログに最小化ボタンを追加する場合、アイコンを描画するための
//  下のコードが必要です。ドキュメント/ビュー モデルを使う MFC アプリケーションの場合、
//  これは、Framework によって自動的に設定されます。

void CMFCDrawDragAreaDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 描画のデバイス コンテキスト

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// クライアントの四角形領域内の中央
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// アイコンの描画
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ユーザーが最小化したウィンドウをドラッグしているときに表示するカーソルを取得するために、
//  システムがこの関数を呼び出します。
HCURSOR CMFCDrawDragAreaDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

/**
 * @brief コントロールの外観が変更された時に呼び出されます。
 *
 * @param [in] nIDCtrl コントロールのID
 * @param [in] lpDrawItemStruct コントロールの情報
 */
void CMFCDrawDragAreaDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	// コントロールのIDにより処理を振り分けます。
	switch (nIDCtl)
	{
	// ピクチャーコントロールの場合
	case IDC_PC_IMAGE:
		{
			// デバイスコンテキストを取得します。
			auto* pDC = m_pcImage.GetDC();

			// マウスの左ボタンが押下中の場合
			if (m_bMouseDown)
			{
				// マウスの左ボタンが押下された座標から移動中の座標まで矩形を描画します。
				CPen pen;
				pen.CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
				auto* pOldPen = pDC->SelectObject(&pen);

				pDC->Rectangle(m_startX, m_startY, m_endX, m_endY);

				pDC->SelectObject(pOldPen);
			}

			// マウスの左ボタンが押下中では無い場合
			else
			{
				// ピクチャーコントロールをクリアします。
				CPen pen;
				pen.CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
				auto* pOldPen = pDC->SelectObject(&pen);

				CBrush brush;
				brush.CreateSolidBrush(RGB(255, 255, 255));
				auto* pOldBrush = pDC->SelectObject(&brush);

				CRect rect;
				m_pcImage.GetClientRect(&rect);
				pDC->Rectangle(&rect);

				pDC->SelectObject(pOldBrush);
				pDC->SelectObject(pOldPen);
			}
		}
		break;

	// 上記以外
	default:
		// 親クラスの関数を呼び出します。
		CDialogEx::OnDrawItem(nIDCtl, lpDrawItemStruct);
		break;
	}
}

/**
 * @brief マウスの左ボタンが押下された時呼び出されます。
 *
 * @param [in] nFlags フラグ
 * @param [in] マウスの左ボタンが押下された座標
 */
void CMFCDrawDragAreaDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// マウスの左ボタンが押下中とします。
	m_bMouseDown = TRUE;

	// マウスのダイアログのクライアント座標をスクリーン座標に変換します。
	ClientToScreen(&point);

	// ピクチャーコントロール上のクライアント座標に変換します。
	m_pcImage.ScreenToClient(&point);

	// 取得した座標を描画の始点として保存します。
	m_startX = point.x;
	m_startY = point.y;

	// マウスをキャプチャーします。
	SetCapture();

	// 親クラスの関数を呼び出します。
	CDialogEx::OnLButtonDown(nFlags, point);
}

/**
 * @brief マウスの左ボタンが離された時呼び出されます。
 *
 * @param [in] nFlags フラグ
 * @param [in] マウスの左ボタンが離された座標
 */
void CMFCDrawDragAreaDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// マウスの左ボタンの押下中を解除します。
	m_bMouseDown = FALSE;

	// マウスのキャプチャーを中止します。
	ReleaseCapture();

	// 描画用の変数をクリアします。
	m_startX = 0.0;
	m_startY = 0.0;
	m_endX = 0.0;
	m_endY = 0.0;

	// ピクチャーコントロールをクリアします。
	CRect rect;
	m_pcImage.GetClientRect(&rect);
	InvalidateRect(rect);

	// 親クラスの関数を呼び出します。
	CDialogEx::OnLButtonUp(nFlags, point);
}

/**
 * @brief マウスが移動中に呼び出されます。
 *
 * @param [in] nFlags フラグ
 * @param [in] マウスの移動中の座標
 */
void CMFCDrawDragAreaDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// マウスの左ボタンが押下中ではない場合
	if (!m_bMouseDown)
	{
		// 何もしません。
		return;
	}

	// マウスのダイアログのクライアント座標をスクリーン座標に変換します。
	ClientToScreen(&point);

	// ピクチャーコントロール上のクライアント座標に変換します。
	m_pcImage.ScreenToClient(&point);

	// マウスの移動中の座標を描画の終点として保存します。
	m_endX = point.x;
	m_endY = point.y;

	// ピクチャーコントロールの描画を更新します。
	CRect rect;
	m_pcImage.GetClientRect(&rect);
	InvalidateRect(rect);

	// 親クラスの関数を呼び出します。
	CDialogEx::OnMouseMove(nFlags, point);
}
